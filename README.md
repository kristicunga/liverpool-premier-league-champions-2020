# LIVERPOOL — PREMIER LEAGUE CHAMPIONS 2020

As a Liverpool fan and open-source enthusiast, I'm so happy to share with you some stuff I'm working for the 19th league title of Liverpool, just for fun. I hope you'll like them and share them with some other dudes 🏆

In the repo you can find some stickers ready for print, and in open files too. You are free to copy and redistribute the material in any medium or format, and remix, transform, and build upon the material as well.

Some other materials like phone backgrounds will be added very soon.

I'm so courios to see how are you using these stuff, so feel free to write me at kristicunga@proton.me.

☕ If you want you can [buy me a coffee](https://www.buymeacoffee.com/kristicunga), or two.

#YNWA